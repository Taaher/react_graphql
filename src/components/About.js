const About = () => {
  const handleLink = () => {
    window.location.assign("https://gitlab.com/Taaher/react_graphql");
  };
  return (
    <div className="about">
      <div className="about-me">
        <div>
          <h5>Developer : Taher zobeydi</h5>
          <h5>
            gitlab:{" "}
            <span onClick={handleLink} className="gitlab">
              open source code!
            </span>
          </h5>
        </div>
      </div>
    </div>
  );
};

export default About;
