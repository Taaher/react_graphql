import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="menu">
      <nav className="navbar">
        <ul>
          <li className="nav-link">
            <NavLink to="/" className="nav-item">
              Home
            </NavLink>
          </li>
          <li className="nav-link">
            <NavLink to="/create" className="nav-item">
              Create
            </NavLink>
          </li>
          <li className="nav-link">
            <NavLink to="/about" className="nav-item">
              About
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
