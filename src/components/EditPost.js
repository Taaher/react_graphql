import { useMutation } from "@apollo/client";
import { useState } from "react";
import { useParams } from "react-router-dom";
import { UPDATE_POST } from "../graphql/Mutations";

const EditPost = () => {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const myId = useParams();

  const [updatePost] = useMutation(UPDATE_POST, {
    onCompleted: () => window.alert("Updated."),
  });

  const onSubmit = (e) => {
    e.preventDefault();
    if (title && body) {
      updatePost({
        variables: {
          id: myId.toString(),
          input: {
            title,
            body,
          },
        },
      });
    }
  };
  return (
    <div className="form-input">
      <form onSubmit={onSubmit}>
        <input
          placeholder="Update title"
          value={title}
          onChange={(e) => {
            setTitle(e.target.value);
          }}
          className="input-post"
        />
        <input
          placeholder="Update body"
          value={body}
          onChange={(e) => {
            setBody(e.target.value);
          }}
          className="input-post"
        />
        <button type="submit" className="btn-post">
          Update
        </button>
      </form>
    </div>
  );
};

export default EditPost;
