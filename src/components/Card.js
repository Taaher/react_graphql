import { FaTrashAlt, FaRegEdit } from "react-icons/fa";
import { DELETE_POST } from "../graphql/Mutations";
import { useMutation } from "@apollo/client";
import { Link } from "react-router-dom";

const Card = ({ id, title, body }) => {
  const [deletePost] = useMutation(DELETE_POST, {
    onCompleted: () => window.alert("Removed."),
  });

  const removePost = (id) => {
    deletePost({
      variables: {
        id,
      },
    });
  };

 
  return (
    <div className="card">
      <div className="title">
        <h6>{title}</h6>
        <div>
          <FaTrashAlt onClick={() => removePost(id)} />
          <Link to={`/update/${id}`}>
            <FaRegEdit />
          </Link>
        </div>
      </div>
      <p>{body}</p>
    </div>
  );
};

export default Card;
