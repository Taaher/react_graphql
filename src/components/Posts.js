import { useQuery } from "@apollo/client";
import { GetPosts } from "../graphql/Queries";
import Card from "./Card";

const Posts = () => {
  const { data, loading, error } = useQuery(GetPosts);
  if (loading)  return <h4 style={{ color: "grey", textAlign: "center" }}>loading...</h4>;
  if (error) return `error : ${error.message}`;

  console.log("checked", data);
  return (
    <div className="App-container">
      {data &&
        data.posts.data.map((post) => {
          return (
            <Card
              id={post.id}
              title={post.title}
              body={post.body}
              key={post.id}
            />
          );
        })}
    </div>
  );
};

export default Posts;
