import { useState } from "react";
import { useMutation } from "@apollo/client";
import { CREATE_POST } from "../graphql/Mutations";

const CreatePost = () => {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [createPost, { loading }] = useMutation(CREATE_POST, {
    onCompleted: (data) => {
      window.alert("saved");
      console.log("saved", data);
    },
  });
  if (loading)
    return <h2 style={{ color: "white", textAlign: "center" }}>loading...</h2>;

  const onSubmit = (e) => {
    e.preventDefault();
    if (title && body) {
      createPost({
        variables: {
          input: {
            title,
            body,
          },
        },
      });
    }
  };

  return (
    <div className="form-group">
      <div className="form-input">
        <form onSubmit={onSubmit}>
          <input
            placeholder="title"
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
            className="input-post"
          />
          <input
            placeholder="body"
            value={body}
            onChange={(e) => {
              setBody(e.target.value);
            }}
            className="input-post"
          />
          <button type="submit" className="btn-post">
            Send
          </button>
        </form>
      </div>
    </div>
  );
};

export default CreatePost;
