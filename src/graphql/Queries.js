import { gql } from "@apollo/client";

export const GetPosts = gql`
  query($options: PageQueryOptions) {
    posts(options: $options) {
      data {
        id
        title
        body
      }
      meta {
        totalCount
      }
    }
  }
`;

