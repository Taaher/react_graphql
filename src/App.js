import "./App.css";
import Posts from "./components/Posts";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Navbar from "./components/Navbar";
import CreatePost from "./components/CreatePost";
import EditPost from "./components/EditPost";
import About from "./components/About";
function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route path="/" exact component={Posts} />
        <Route path="/create" component={CreatePost} />
        <Route path="/update/:id" component={EditPost} />
        <Route path="/about" component={About} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
